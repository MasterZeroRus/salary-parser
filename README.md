## Salary calculator

Main logic placed at `app/Helpers`. Example usage inside `app/Http/Controllers/SalaryController`.

To apply changes to your project follow this steps:
- Copy `app/Helpers/*` to your app
- register singleton `HolidaysRepo` as it made in `app/Providers/AppServiceProvider`
- require laravel http client via `composer require guzzlehttp/guzzle`
- copy and run unit test from `tests/Unit/SalaryTest` (optional)



### Installing sandbox

Clone this repo and then follow commands:
```
composer install
php artisan serve
```
