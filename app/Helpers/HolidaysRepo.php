<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class HolidaysRepo
{
    public const CACHE_TIMEOUT_MINUTES = 10;
    public const CACHE_KEY_BASE = 'calendar_holidays_';
    protected $holidays = [];

    public function isHoliday(int $year, int $month, int $day)
    {
        return in_array($day, $this->holidays[$year][$month]);
    }

    public function count(int $year, int $month)
    {
        return count($this->holidays[$year][$month]);
    }

    public function load(int $year)
    {
        if (Cache::has(self::CACHE_KEY_BASE.$year)) {
            return $this->holidays[$year] = Cache::get(self::CACHE_KEY_BASE.$year);
        }

        $response = Http::get('http://xmlcalendar.ru/data/ru/'.$year.'/calendar.csv');
        $body = $response->body();
        $this->holidays[$year] = $this->fromCsv($body);
        Cache::put(
            self::CACHE_KEY_BASE.$year,
            $this->holidays[$year],
            now()->addMinutes(self::CACHE_TIMEOUT_MINUTES));
    }

    protected function fromCsv(string $data)
    {
        $match = [];
        // get line of days
        $success = preg_match('/\\n\\d{4},((?:"(?:\\d+[*+]?,)*\\d+[*+]?",?){12})/', $data, $match);

        if (!$success) {
            throw new Exception('invalid line data of csv input file');
        }

        // parse month
        $monthMatches = [];
        $success = preg_match_all('/"((?:\\d+[+*]?,)+\\d+[+*]?)"/', $match[1], $monthMatches);

        if (!$success) {
            throw new Exception('invalid month data of csv input file');
        }

        $ret = [];

        // parse days
        foreach ($monthMatches[1] as $monthId => $days) {
            $ret[$monthId + 1] = $this->daysFromString($days);
        }

        if (count($ret) != 12) {
            throw new Exception('invalid count of month in csv input file');
        }

        return $ret;
    }

    // string "1,2*,3,4+,5" =>  array [1,3,4,5]
    protected function daysFromString(string $days): array
    {
        $daysArr = explode(',', $days);

        $ret = [];
        foreach ($daysArr as $day) {
            $len = strlen($day);
            $lastChar = $day[$len - 1];
            if ($lastChar === '*') {
                continue;
            }

            if ($lastChar === '+') {
                $day = substr($day, 0, $len - 1);
            }

            $ret[] = (int) $day;
        }

        return $ret;
    }
}
