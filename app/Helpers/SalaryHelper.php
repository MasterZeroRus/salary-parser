<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Validator;

class SalaryHelper
{
    protected HolidaysRepo $holidaysRepo;
    protected int $monthlySalary;
    protected array $start;
    protected array $end;

    public function __construct()
    {
        $this->holidaysRepo = app(HolidaysRepo::class);
    }

    /**
     * getPartialSalary.
     *
     * @param string $startDate in date('Y-m-d') format. Example: 2022-01-10
     * @param string $endDate   in date('Y-m-d') format. Example: 2022-04-10
     */
    public function getPartialSalary(int $monthlySalary, string $startDate, string $endDate): int
    {
        $validator = Validator::make([
            'salary' => $monthlySalary,
            'start' => $startDate,
            'end' => $endDate,
        ], [
            'salary' => 'gt:0|integer',
            'start' => 'date|date_format:Y-m-d',
            'end' => 'gte:start|date|date_format:Y-m-d',
        ])->validate();

        $this->monthlySalary = $monthlySalary;
        $this->start = $this->toDateArray($startDate);
        $this->end = $this->toDateArray($endDate);

        return $this->calc();
    }

    protected function calc()
    {
        $total = 0;

        for ($year = $this->start['year']; $year <= $this->end['year']; ++$year) {
            $this->holidaysRepo->load($year);
            $startMonth = 1;
            $endMonth = 12;

            if ($year == $this->start['year']) {
                $startMonth = $this->start['month'];
            }

            if ($year == $this->end['year']) {
                $endMonth = $this->end['month'];
            }

            for ($month = $startMonth; $month <= $endMonth; ++$month) {
                $salary = $this->salary($year, $month);
                $total += $salary;
            }
        }

        // @TODO: test it better
        return $total;
    }

    protected function salary(int $year, int $month): int
    {
        $isStartMonth = $year == $this->start['year'] && $month == $this->start['month'];
        $isEndMonth = $year == $this->end['year'] && $month == $this->end['month'];
        $holidaysCount = $this->holidaysRepo->count($year, $month);
        $daysInMonth = $this->daysInMonth($year, $month);
        $shouldWorkDays = $daysInMonth - $holidaysCount;
        $actuallyWorkDays = $shouldWorkDays;

        if ($isStartMonth || $isEndMonth) {
            $dayFrom = 1;
            $dayTo = $daysInMonth;
            if ($isStartMonth) {
                $dayFrom = $this->start['day'];
            }
            if ($isEndMonth) {
                $dayTo = $this->end['day'];
            }
            $actuallyWorkDays = $this->actuallyWorkingDays($year, $month, $dayFrom, $dayTo);
        }

        return ceil((float) $actuallyWorkDays / (float) $shouldWorkDays * $this->monthlySalary);
    }

    protected function daysInMonth(int $year, int $month): int
    {
        return date('t', strtotime($year.'-'.$month));
    }

    protected function actuallyWorkingDays(int $year, int $month, int $startDay, int $endDay): int
    {
        $workingDays = 0;
        for ($day = $startDay; $day <= $endDay; ++$day) {
            if ($this->holidaysRepo->isHoliday($year, $month, $day)) {
                continue; // skip holidays
            }
            ++$workingDays;
        }

        return $workingDays;
    }

    // string "2022-01-10" => array ['year' => 2022, 'month' => 1, 'day' => 10]
    protected function toDateArray(string $inputDate): array
    {
        $match = [];
        $success = preg_match('/^(\\d{4})-(\\d{2})-(\\d{2})$/', $inputDate, $match);

        return [
            'year' => (int) $match[1],
            'month' => (int) $match[2],
            'day' => (int) $match[3],
        ];
    }
}
