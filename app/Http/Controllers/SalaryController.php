<?php

namespace App\Http\Controllers;

use App\Helpers\SalaryHelper;
use Illuminate\Http\JsonResponse;

class SalaryController extends Controller
{
    public function calc(): JsonResponse
    {
        $salaryHelper = new SalaryHelper();

        $salary = $salaryHelper->getPartialSalary('50000', '2021-10-10', '2022-02-12');

        return response()->json($salary);
    }
}
