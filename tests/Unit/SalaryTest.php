<?php

namespace Tests\Unit;

use App\Helpers\SalaryHelper;
use Tests\TestCase;

class SalaryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $salaryHelper = new SalaryHelper();
        $this->assertEquals(64, $salaryHelper->getPartialSalary(100, '2022-02-10', '2022-02-28'));
    }
}
